@extends('layouts.app')

@section('content')
<div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="content">
                <div class="col-md-4 col-md-offset-4">
                    {!! Alert::render() !!}
                </div>
                    <div class="title">
                        Laravel

                    </div>

                    <div class="col-md-6 col-md-offset-3">
                    @if(date('Y-m-d') <= @$subscription->date && @$subscription->type_payments > 0)
                        <h3>Tu suscripcion es Premium</h3>
                    @else
                        <h3>Tu suscripcion es basica</h3>
                        <div class="links">
                            <a data="{{url('/subscription/'.@$subscription->id.'/edit')}}" id="create" >Actualizar suscripcion</a>
                        </div>
                    @endif


                    </div>
                </div>
                <div id="contenido">
                    @section('contentpanel')
                    @show
                    <div class="col-md-6 col-md-offset-3">
                        <table class="table table-snipet">
                        <tr>
                            <th>Nº de Suscripcion</th>
                            <th>Fecha Vencimiento</th>
                            <th>Tipo de pago</th>
                        </tr>

                        <tr>
                            <td>{{@$subscription->id}}</td>
                            <td>{{@$subscription->date}}</td>
                            <td>{{typePayment(@$subscription->type_payments)}}</td>
                        </tr>
                    </table>
                    </div>

                </div>

            </div>
        </div>
    </div>
@endsection
