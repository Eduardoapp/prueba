@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    {!! Form::open(['url' => '/register' ]) !!}
                        {{ csrf_field() }}
                        {!! Field::text('nombre', ['ph'=>'Nombre'])!!}
                        {!! Field::text('apellido', ['ph'=>'Apellido'])!!}
                        {!! Field::text('telefono', ['ph'=>'Télefono'])!!}
                        {!! Field::email('email', ['ph'=>'Correo electronico']) !!}
                        {!! Field::password('password', ['ph'=>'Contraseña','id'=>'password']) !!}
                        {!! Field::password('password_confirmation', ['ph'=>'Confirmar contraseña','id'=>'password-confirm']) !!}
                        {!! Form::submit('Ingresar', ['class' => 'btn btn-success col-md-12']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
