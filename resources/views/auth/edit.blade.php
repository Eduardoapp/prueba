@extends('layouts.app')

@section('contentpanel')
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">Editar</div>
                <div class="panel-body">
                    {!! Form::open(['url' => '/user/'.$data->id , 'method'=>'put' ]) !!}
                        {{ csrf_field() }}
                        {!! Field::text('nombre',$data->nombre, ['ph'=>'Nombre'])!!}
                        {!! Field::text('apellido',$data->apellido, ['ph'=>'Apellido'])!!}
                        {!! Field::text('telefono',$data->telefono, ['ph'=>'Téfono'])!!}
                        {!! Field::email('email',$data->email, ['ph'=>'Correo electronico']) !!}
                        {!! Form::submit('Ingresar', ['class' => 'btn btn-success col-md-12']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
