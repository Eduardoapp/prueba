@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<div class="panel panel-default">
				<div class="panel-heading">Iniciar Sesion</div>
				<div class="panel-body">
					{!! Form::open() !!}
						{{ csrf_field() }}
						{!! Field::email('email', ['ph'=>'Correo electronico']) !!}
						{!! Field::password('password', ['ph'=>'Contraseña']) !!}
						{!! Form::submit('Ingresar', ['class' => 'btn btn-success col-md-12']) !!}
					{!! Form::close() !!}
				</div>
		</div>
		</div>
	</div>
</div>
@endsection
