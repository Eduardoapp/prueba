@extends('layouts.app')
@section('contentPanel')
<table class="table table-bordered">
    <tr>
        <th>Nº Susccripcion</th>
        <th>Fecha Vencimiento</th>
        <th>Tipo de pago</th>
    </tr>
    @foreach($subscription as $suscriptions)
    <tr>
        <td>{{$suscriptions->id}}</td>
        <td>{{$suscriptions->date}}</td>
        <td>{{$suscriptions->type_payments}}</td>
    </tr>
    @endforeach
</table>
@endsection