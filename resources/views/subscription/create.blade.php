@extends('layouts.app')
@section('contentpanel')
<div class=".bootstrap-iso">
	<div class="container">
	<div class="row">
	    <div class="col-md-4 col-md-offset-4">
	        <div class="panel panel-default">
	            <div class="panel-heading">Actualizar Suscripcion</div>
	            <div class="panel-body ">
	                {!! Form::open(['url' => '/subscription']) !!}
	                    {{ csrf_field() }}

		                
					      <label class="control-label requiredField" for="date">
					       Date
					      </label>
					       <div class="input-group">
					        
					        	<input class="form-control" id="date" name="date" placeholder="MM/DD/YYYY" type="text"/>
					        	<div class="input-group-addon">
						         <i class="glyphicon glyphicon-calendar">
						         </i>
						        </div>
					       </div>
							<label>Tipo de pago</label>
	                    	<select class="form-control input-lg">
	                    		<option>Seleccione</option>
	                    		@foreach($data as $dat)
	                    			<option value="{{$dat->id}}">{{$dat->type}}</option>
	                    		@endforeach
	                    	</select>
	                    	<br>

	                    	<input type="hidden" name="user" value="">
	                    {!! Form::submit('Ingresar', ['class' => 'btn btn-success col-md-12']) !!}
	                {!! Form::close() !!}
	            </div>
	        </div>
	    </div>
	</div>
</div>
</div>


<script type="text/javascript">
	$(document).ready(function(){
        var date_input=$('input[name="date"]'); //our date input has the name "date"
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        date_input.datepicker({
            format: 'mm/dd/yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
        })
    })
</script>
@endsection