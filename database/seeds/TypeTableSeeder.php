<?php

use Illuminate\Database\Seeder;
use App\Type_payment;

class TypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('type_payments')->insert([
          ['type'=>'Efectivo'],
          ['type'=>'Credito']
        ]);
    }
}
