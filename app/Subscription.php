<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $fillable = [

    	'id',
    	'user',
    	'date',
    	'type_payments',

    ];

    public function scopeUSer($query, $id)
    {
    	return $query->where('user',$id)->first();
    }
}
