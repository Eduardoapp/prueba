<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type_payment extends Model
{
    protected $fillable = [

    	'id',
    	'type',

    ];

    protected $table ='type_payments';
}
