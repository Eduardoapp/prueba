<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subscription;
use App\Type_payment;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $var = \Auth::user()->id;
        if ($var < 1) {
          return redirect('/login');
        }else{
          $subscription = Subscription::user($var);
          return view('home',compact('subscription'));
        }



    }
}
