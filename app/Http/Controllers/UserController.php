<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Subscription;

class UserController extends Controller
{
    public function edit($id)
    {
    	$data = user::find($id);
        $view = \View::make('auth.edit',compact('data'));
        if(\Request()->ajax()) {
        $sections = $view->renderSections();
        return json_encode($sections);

        }
        return $view;
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);

        if ($user->nombre   == $request['nombre']&&
            $user->apellido == $request['apellido']&&
            $user->telefono == $request['telefono']&&
            $user->email    == $request['email']) {

            $subscription = Subscription::user($user->id);
            \Alert::success('No ser realizaron cambios');
            return redirect('/home')->with('subscription',$subscription);

        }else{

            $user->nombre   = $request['nombre'];
            $user->apellido = $request['apellido'];
            $user->telefono = $request['telefono'];
            $user->email    = $request['email'];

            $user->save();

            $subscription = Subscription::user($user->id);
            \Alert::success('Datos actualizados correctamente');
            return redirect('/home')->with('subscription',$subscription);
        }
    }

    public function store(Request $request)
    {
      $user = new User();

      $user->nombre   = $request['nombre'];
      $user->apellido = $request['apellido'];
      $user->telefono = $request['telefono'];
      $user->email    = $request['email'];
      $user->password = bcrypt($request['password']);

      $user->save();

      return redirect('/home');
    }
}
