<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;
use App\Type_payment;
use App\Subscription;
use App\User;


class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {



    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data = type_payment::all();
        $view = View::make('subscription.create',compact('data'));
        if(Request()->ajax()) {
        $sections = $view->renderSections();
        return json_encode($sections);

        }
        return $view;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    public function edit($id)
    {
        $data = Subscription::find($id);
        $data2 = Type_payment::all();
        $iduser = \Auth::user()->id;
        $user = User::find($iduser);
        $view = View::make('subscription.edit',compact('data','data2','user'));
        if(Request()->ajax()) {
        $sections = $view->renderSections();
        return json_encode($sections);

        }
        return $view;
    }

    public function update(Request $request, $id)
    {


         $subscription = Subscription::find($id);

        if ($subscription->user          == $request['user']&&
            $subscription->date          == $request['date']&&
            $subscription->type_payments == $request['type']) {

            $subscription = Subscription::user($request['user']);
            \Alert::success('No ser realizaron cambios');
            return redirect('/home')->with('subscription',$subscription);

        }else{

            $subscription->user         = $request['user'];
            $subscription->date         = $request['date'];
            $subscription->type_payments = $request['type'];

            $subscription->save();

            $subscription = Subscription::user($request['user']);
            \Alert::success('Cambios realizados exitosamente');
            return redirect('/home')->with('subscription',$subscription);

        }

    }


}
